### Description

You need to develop a web-application which will list GitHub repositories and commits for a given repo. 

We expect you’ll display a form with :owner/:repo field and a button that will initialize an import of selected repo to local database. You need to implement importing as many repos as you want.
 
Repos should be organized as a list.
 
Filtering by branch is not necessary.
 
You need to display 20 commits on each page under a given repo. You need to implement a mass deletion of commits.
 
Next import should restore deleted commits, avoid commits duplicates.

Repo can also be imported via CLI command, commits can be viewed via CLI command also.

##### Goals

    List commits for a given repo.
    Separate component for rendering commits with repos.
    CLI command to import repo and display commits.
    REST API for creating/listing/deleting repos.
    SPA app without page reloading for any action.

##### Requirements

    PHP7
    Laravel/Symfony
    Any SQL/NoSQL DB
    Any frontend framework

##### Acceptance Criteria

Should use DB to persist repos and commits

No requirements for DB’s choice

REST API method / CLI command / web page to list commits

You may display commits in CLI mode as you want. On a web page you need a control for mass deletion of commits.

Deletion of commits \
You need to create a separate button for group deletion.

Unit tests \
You may write only backend tests.
