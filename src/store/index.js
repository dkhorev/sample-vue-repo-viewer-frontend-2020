import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {
  GET_REPOSITORIES,
  GET_COMMITS,
  LOADING,
  IMPORT_REPOSITORY,
  REQUEST_LAST_ERROR,
  SET_REPO_FILTERS,
  SET_COMMIT_FILTERS,
  DELETE_COMMITS,
} from './actions';
import getUrl from './utils';

Vue.use(Vuex);

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common.accept = 'application/json';

export default new Vuex.Store({
  state: {
    repositories: {
      data: [],
      pagination: [],
    },
    commits: {},
    loading: {},
    requestErrors: {},
    repoListFilters: {
      per_page: 5,
      page: 1,
    },
    commitListFilters: {
      per_page: 10,
      page: 1,
    },
    path: {
      [GET_REPOSITORIES]: 'api/v1/repo',
      [GET_COMMITS]: 'api/v1/repo/:id/commits',
      [DELETE_COMMITS]: 'api/v1/repo/:id/commits',
      [IMPORT_REPOSITORY]: 'api/v1/repo',
    },
  },

  getters: {
    repositories: state => state.repositories,
    commits: state => state.commits,
    loading: state => state.loading,
    requestErrors: state => state.requestErrors,
    repoListFilters: state => state.repoListFilters,
    commitListFilters: state => state.commitListFilters,
  },

  mutations: {
    [SET_REPO_FILTERS]: (state, { data }) => {
      Vue.set(state, 'repoListFilters', data);
    },

    [SET_COMMIT_FILTERS]: (state, { data }) => {
      Vue.set(state, 'commitListFilters', data);
    },

    [LOADING]: (state, { name, value }) => {
      Vue.set(state.loading, name, value);
    },

    [GET_REPOSITORIES]: (state, { data }) => {
      Vue.set(state.repositories, 'data', data.data);
      Vue.set(state.repositories, 'pagination', data.meta);
    },

    [GET_COMMITS]: (state, { data }) => {
      Vue.set(state.commits, 'data', data.data);
      Vue.set(state.commits, 'pagination', data.meta);
    },

    [REQUEST_LAST_ERROR]: (state, { name, value }) => {
      Vue.set(state.requestErrors, name, value);
    },
  },

  actions: {
    [SET_REPO_FILTERS]: ({ commit }, { data }) => {
      commit(SET_REPO_FILTERS, { data });
    },

    [SET_COMMIT_FILTERS]: ({ commit }, { data }) => {
      commit(SET_COMMIT_FILTERS, { data });
    },

    [IMPORT_REPOSITORY]: async ({ state, commit, dispatch }, { data }) => {
      commit(LOADING, {
        name: IMPORT_REPOSITORY,
        value: true,
      });
      commit(REQUEST_LAST_ERROR, {
        name: IMPORT_REPOSITORY,
        value: false,
      });

      try {
        await axios.post(getUrl(state.path[IMPORT_REPOSITORY]), data);
        await dispatch(GET_REPOSITORIES);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, {
          name: IMPORT_REPOSITORY,
          value: e.response.data.message,
        });
      }

      commit(LOADING, {
        name: IMPORT_REPOSITORY,
        value: false,
      });
    },

    [GET_REPOSITORIES]: async ({ state, commit }) => {
      commit(LOADING, {
        name: GET_REPOSITORIES,
        value: true,
      });
      commit(REQUEST_LAST_ERROR, {
        name: GET_REPOSITORIES,
        value: false,
      });

      try {
        const response = await axios.get(getUrl(state.path[GET_REPOSITORIES]), { params: state.repoListFilters });
        commit(GET_REPOSITORIES, response);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, {
          name: GET_REPOSITORIES,
          value: e.response.data.message,
        });
      }

      commit(LOADING, {
        name: GET_REPOSITORIES,
        value: false,
      });
    },

    [GET_COMMITS]: async ({ state, commit }, { id }) => {
      commit(LOADING, {
        name: GET_COMMITS,
        value: true,
      });
      commit(REQUEST_LAST_ERROR, {
        name: GET_COMMITS,
        value: false,
      });

      try {
        const response = await axios.get(getUrl(state.path[GET_COMMITS], id), { params: state.commitListFilters });
        commit(GET_COMMITS, response);
      } catch (e) {
        commit(REQUEST_LAST_ERROR, {
          name: GET_COMMITS,
          value: e.response.data.message,
        });
      }

      commit(LOADING, {
        name: GET_COMMITS,
        value: false,
      });
    },

    [DELETE_COMMITS]: async ({ dispatch, state, commit }, { commits, id }) => {
      commit(LOADING, {
        name: DELETE_COMMITS,
        value: true,
      });
      commit(REQUEST_LAST_ERROR, {
        name: DELETE_COMMITS,
        value: false,
      });

      try {
        await axios.delete(getUrl(state.path[DELETE_COMMITS], id), { params: { commits } });
        await dispatch(GET_COMMITS, { id });
      } catch (e) {
        commit(REQUEST_LAST_ERROR, {
          name: DELETE_COMMITS,
          value: e.response.data.message,
        });
      }

      commit(LOADING, {
        name: DELETE_COMMITS,
        value: false,
      });
    },
  },

  modules: {},
});
