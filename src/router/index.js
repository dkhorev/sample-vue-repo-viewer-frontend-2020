import Vue from 'vue';
import VueRouter from 'vue-router';
import Repositories from '../views/Repositories.vue';
import Project from '../views/Project.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Repositories',
    component: Repositories,
  },
  {
    path: '/project/:id',
    name: 'Project',
    component: Project,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
